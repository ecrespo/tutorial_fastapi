import time
from enum import Enum
import uuid
from typing import Union

from fastapi import (
    APIRouter,
    Security,
    HTTPException,
    Path,
    Query,
    Header,
    Cookie,
    Body,
    Form,
    File,
    UploadFile
)

from celery import chord

from app.celery_tasks.task import io_bound_task, collect_results
from app.middlewares.verify_api_key import APIKeyVerifier
from app.utils.LoggerSingleton import logger
from app.utils.configs import API_KEY_AUTH


router = APIRouter(
    tags=["PasteBin"],
)

api_key_verifier = APIKeyVerifier(API_KEY_AUTH)


@router.get("/sequential")
def sequential_task():
    start_time = time.time()
    urls = ["https://httpbin.org/delay/3" for _ in range(10)]
    results = []
    for url in urls:
        results.append(io_bound_task(url))
    end_time = time.time()
    return {"status": results, "time taken": end_time - start_time}


@router.get("/sequential_celery")
def celery_task():
    start_time = time.time()
    urls = ["https://httpbin.org/delay/3" for _ in range(10)]
    tasks = [io_bound_task.s(url) for url in urls]
    callback = chord(tasks)(collect_results.s(start_time))
    return {"task_id": callback.id}


@router.get("/result/{task_id}")
def get_result(task_id: str):
    task = collect_results.AsyncResult(task_id)
    logger.info(f"Task state: {task}")
    if task.state == 'PENDING':
        return {"status": "Task is still in progress", "state": task.state}
    elif task.state != 'FAILURE':
        return task.result
    else:
        return {"status": "Task failed", "state": task.state, "error": str(task.info)}