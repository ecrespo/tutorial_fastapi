from enum import Enum
import uuid
from typing import Union
from typing import List
from beanie import PydanticObjectId
from bson import ObjectId


from fastapi import (
    APIRouter,
    Security,
    HTTPException,
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_401_UNAUTHORIZED,
    HTTP_403_FORBIDDEN,
    HTTP_404_NOT_FOUND,
    HTTP_405_METHOD_NOT_ALLOWED,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from app.models.Task import Task

from app.middlewares.verify_api_key import APIKeyVerifier
from app.repositories.MongoTaskRepository import MongoTaskRepository
from app.utils.LoggerSingleton import logger
from app.utils.configs import API_KEY_AUTH, ROOT_PASSWORD, ROOT_USERNAME, MONGO_URI


task_router = APIRouter(
    tags=["Task"],
)

task_repository = MongoTaskRepository()
api_key_verifier = APIKeyVerifier(API_KEY_AUTH)


@task_router.get("/", status_code=HTTP_200_OK)
async def getalltasks() -> List[Task]:
    tasks = await task_repository.get_all_tasks()
    return tasks


@task_router.post("/", status_code=HTTP_201_CREATED)
async def createTask(task: Task):
    await task_repository.create_task(task)
    return {"message": "Task has been saved"}


@task_router.get("/{task_id}", status_code=HTTP_200_OK)
async def retrieveTask(task_id: PydanticObjectId) -> Task:
    task = await task_repository.get_task(task_id)
    if not task:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Task not found")
    return task


@task_router.put("/{task_id}", status_code=HTTP_200_OK)
async def updateTask(task: Task, task_id: PydanticObjectId) -> Task:
    updated_task = await task_repository.update_task(task_id, task)
    if not updated_task:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Task not found")
    return updated_task


@task_router.delete("/{task_id}", status_code=HTTP_204_NO_CONTENT)
async def deleteTask(task_id: PydanticObjectId):
    await task_repository.delete_task(task_id)
    return {"message": "Task deleted"}
