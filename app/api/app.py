from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.utils.LoggerSingleton import logger

from app.api.tutorial.app import router as api_router
from app.api.GraphQL.app import graphql_app
from app.api.PasteBin.app import router as paste_bin_router
from app.utils.database import DatabaseConnection

app = FastAPI(
    title="Tutorial de FastAPI",  # The title of the API
    description="Tutorial de FastAPI",  # The description of the API
    version="0.1.0",  # The version of the API
    docs_url="/docs",  # The URL where the API documentation will be served
    redoc_url=None,  # The URL where the ReDoc documentation will be served. None means it will not be served
    cors_allowed_origins="*",  # The origins that are allowed to make cross-origin requests
    cors_allowed_methods="*", # Allow all methods
    allow_credentials=True,
    allow_headers="*",
)

# Add a middleware for handling CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Allow all origins
    allow_credentials=True,
    allow_methods=["*"],  # Allow all methods
    allow_headers=["*"],  # Allow all headers
)


@app.get("/")
def read_root() -> dict:
    logger.info("read_root")
    return {
        "title": app.title,
        "version": app.version,
    }


@app.on_event("startup")
async def connect():
    logger.info("connecting to database")
    db_connection = DatabaseConnection()
    logger.info("Database connection successful")
    await db_connection.init_db()


app.include_router(api_router, prefix="/api/v1/tutorial")
app.include_router(paste_bin_router, prefix="/api/v1/paste_bin")
app.include_router(graphql_app, prefix="/graphql")
logger.info("API started")