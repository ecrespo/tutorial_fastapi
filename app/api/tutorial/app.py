from enum import Enum
import uuid
from typing import Union

from fastapi import (
    APIRouter,
    Security,
    HTTPException,
    Path,
    Query,
    Header,
    Cookie,
    Body,
    Form,
    File,
    UploadFile
)

from app.middlewares.verify_api_key import APIKeyVerifier
from app.utils.LoggerSingleton import logger
from app.utils.configs import API_KEY_AUTH


router = APIRouter(
    tags=["tuto"],
)

api_key_verifier = APIKeyVerifier(API_KEY_AUTH)
REGEX_TZ = r"^GMT[+-]((0?[0-9]|1[0-1]):([0-5][0-9])|12:00)$"


@router.get("/prueba", summary="Prueba de API")
def prueba(api_key: str = Security(api_key_verifier)) -> dict:
    logger.info("prueba", extra={"api_key": api_key})
    return {"message": "Prueba exitosa"}


@router.get("/tutorial/{_id}")
async def read_item(
    _id: int = Path(gt=0,
                   title="Blog ID",
                   description="tutorial resource identifier"),):
    logger.info("prueba", extra={"_id": _id})
    return {"_id": _id}


class TutorialOrder(str, Enum):
    age = "age"
    title = "title"
    created_at = "created_at"


@router.get("/tutorial/")
async def read_items(order_by: TutorialOrder = Query(default=TutorialOrder.created_at)):
    return {"order_by": order_by}


@router.get("/tutorials/")
async def read_items(client_tz: str = Header(pattern=REGEX_TZ)):
    return {"Client-TZ": client_tz}


@router.get("/tutoriales/")
async def read_items(country: str = Cookie(default=None)):
    # document.cookie='country=Brazil'
    return {"Country": country}

@router.post("/tutorial/")
async def read_items(content: str = Body(...)):
    return {"Content": content}


@router.post("/tutorials/")
async def read_items(user: str = Form(...), password: str = Form(...)):
    return {"user": user, "password": password}


@router.post("/blog/")
async def read_items(photo: UploadFile = File(...)):
    filename = f"/tmp/{uuid.uuid4()}-{photo.filename}"

    with open(filename, "wb") as f:
        while contents := photo.file.read(1024 * 1024):
            f.write(contents)

    return {"Photo": filename}


@router.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}

