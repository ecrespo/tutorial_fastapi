from pydantic_settings import BaseSettings, SettingsConfigDict
from functools import lru_cache

from app.utils.LoggerSingleton import logger


# Define a Settings class that inherits from BaseSettings
class Settings(BaseSettings):
    # Define the attributes of the Settings class
    API_KEY_AUTH: str
    MONGO_URI: str
    ROOT_USERNAME: str
    ROOT_PASSWORD: str

    # Load the settings from the .env file
    model_config = SettingsConfigDict(env_file=".env")


@lru_cache()
def get_settings() -> Settings:
    """
        This function returns an instance of the Settings class.
        It uses the lru_cache decorator to cache the result,
        so that subsequent calls do not have to re-instantiate the Settings class.
    """
    return Settings()


# Get the settings
settings = get_settings()

# Assign the settings to variables
API_KEY_AUTH = settings.API_KEY_AUTH
MONGO_URI = settings.MONGO_URI
ROOT_USERNAME = settings.ROOT_USERNAME
ROOT_PASSWORD = settings.ROOT_PASSWORD
logger.info("Settings loaded")
