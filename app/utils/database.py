import motor.motor_asyncio
from beanie import init_beanie
from app.models.Task import Task
from app.utils.configs import MONGO_URI
from app.utils.LoggerSingleton import logger


# async def init_db():
#     logger.info("Connecting to the Mongo database")
#     client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_URI)
#
#     await init_beanie(database=client.db_name, document_models=[Task])


class DatabaseConnection:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(DatabaseConnection, cls).__new__(cls)
            logger.info("Creating a new database connection instance")
        return cls._instance

    async def init_db(self):
        logger.info("Connecting to the Mongo database")
        client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_URI)
        await init_beanie(database=client.db_name, document_models=[Task])
        logger.info("Database connection and Beanie initialization successful")



