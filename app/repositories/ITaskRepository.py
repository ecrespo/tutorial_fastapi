from typing import List
from abc import ABC, abstractmethod
from app.models.Task import Task
from beanie import PydanticObjectId


class ITaskRepository(ABC):

    @abstractmethod
    async def get_all_tasks(self) -> List[Task]:
        pass

    @abstractmethod
    async def create_task(self, task: Task):
        pass

    @abstractmethod
    async def get_task(self, task_id: PydanticObjectId) -> Task:
        pass

    @abstractmethod
    async def update_task(self, task_id: PydanticObjectId, task: Task) -> Task:
        pass

    @abstractmethod
    async def delete_task(self, task_id: PydanticObjectId):
        pass