from typing import List

from app.repositories.ITaskRepository import ITaskRepository
from app.models.Task import Task
from beanie import PydanticObjectId


class MongoTaskRepository(ITaskRepository):

    async def get_all_tasks(self) -> List[Task]:
        tasks = await Task.find_all().to_list()
        return tasks

    async def create_task(self, task: Task):
        await task.create()

    async def get_task(self, task_id: PydanticObjectId) -> Task:
        task = await Task.get(task_id)
        return task

    async def update_task(self, task_id: PydanticObjectId, task_data: Task) -> Task:
        task = await Task.get(task_id)
        task.task_content = task_data.task_content
        task.is_complete = task_data.is_complete
        await task.save()
        return task

    async def delete_task(self, task_id: PydanticObjectId):
        task = await Task.get(task_id)
        await task.delete()